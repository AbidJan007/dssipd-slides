# DSSIPD SoSe 2022

## Course slides

The slides of this course are tied directly to the Markdown (i.e. `.md` files) in this repository.

## List of presentations

1. Laptop setup slides [HTML](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/01-laptop-setup.html)/[PDF](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/01-laptop-setup.pdf).
1. Markdown slides [HTML](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/02-markdown.html)/[PDF](https://rwth-crmasters-sose22.gitlab.io/dssipd-slides/02-markdown.pdf).
